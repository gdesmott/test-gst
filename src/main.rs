use std::sync::atomic::{AtomicBool, Ordering};

use gst::prelude::*;

const MKV: &str = "https://gitlab.freedesktop.org/gdesmott/karapulse/-/raw/main/tests/medias/video-2s.mkv?inline=false";
const FAIL: &str = "file://not-there.ogg";

fn main() -> anyhow::Result<()> {
    gst::init()?;

    let pipeline = gst::ElementFactory::make("playbin3").build()?;
    pipeline.set_property("uri", MKV);

    let restarted = AtomicBool::new(false);
    pipeline.connect("about-to-finish", false, move |args| {
        let uridecodebin = args[0].get::<gst::Element>().unwrap();

        if !restarted.load(Ordering::Relaxed) {
            let uri = FAIL;
            println!("finish, next {}", uri);
            uridecodebin.set_property("uri", uri);

            restarted.store(true, Ordering::Relaxed);
        } else {
            println!("finish done");
        }

        None
    });

    let bus = pipeline.bus().unwrap();
    pipeline.set_state(gst::State::Playing)?;

    for msg in bus.iter_timed(gst::ClockTime::NONE) {
        use gst::MessageView;

        match msg.view() {
            MessageView::Eos(..) => {
                dbg!("eos");
                break;
            }
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.src().map(|s| s.path_string()),
                    err.error(),
                    err.debug()
                );
                break;
            }

            _ => (),
        }
    }

    let res = pipeline.set_state(gst::State::Null);
    dbg!(&res);
    res.unwrap();

    Ok(())
}
